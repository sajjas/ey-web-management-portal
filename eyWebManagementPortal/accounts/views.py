from django.shortcuts import render, redirect, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_control
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import User
from django.contrib import messages
from django.urls import reverse
from .forms import UsersLoginForm, UsersRegisterForm


def login_view(request):
	form = UsersLoginForm(request.POST or None)
	if form.is_valid():
		username = form.cleaned_data.get("username")
		password = form.cleaned_data.get("password")
		user = authenticate(username = username, password = password)
		login(request, user)
		return redirect("home")
	return render(request, "accounts/form.html", {
		"form" : form,
		"title" : "Login",
	})

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='login') #if not logged in redirect to /login
def logout_view(request):
    logout(request)
    # Redirect back to index page.
    return HttpResponseRedirect('/')

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='login') #if not logged in redirect to /login
def register_view(request):
	form = UsersRegisterForm(request.POST or None)
	if form.is_valid():
		user = form.save()
		password = form.cleaned_data.get("password")	
		user.set_password(password)
		user.save()
		# new_user = authenticate(username = user.username, password = password)
		# login(request, new_user)
		return redirect("home")
	return render(request, "accounts/form.html", {
		"title" : "Register",
		"form" : form,
	})

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='login') #if not logged in redirect to /login
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return HttpResponseRedirect(reverse('home'))
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'accounts/change_password.html', {
        'form': form
    })

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='login') #if not logged in redirect to /login
def list_users_view(request):
	user_list = User.objects.values()
	return render(request, "accounts/list_users.html", {"user_list": user_list})
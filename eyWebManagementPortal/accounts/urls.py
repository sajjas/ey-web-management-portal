from django.conf.urls import url
from django.urls import path
from .views import (
    login_view,
    logout_view,
    change_password,
    register_view,
    list_users_view,
)

urlpatterns = [
    url(r"^login/$", login_view, name = "login"),
    url(r"^logout/$", logout_view, name="logout"),
    url(r"^register/$", register_view, name="register"),
    url(r"^list_users/$", list_users_view, name="list_users"),
    url(r"^change_password/$", change_password, name="change_password"),
]
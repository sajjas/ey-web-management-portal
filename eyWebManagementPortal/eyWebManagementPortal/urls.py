"""testdjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.contrib.auth import views as auth_views
from backupDatabase.views import (
    home,
    db_backup,
    ajax_load_db_instances,
    ajax_load_applications,
    ajax_load_databases
)


urlpatterns = [
    # ... other urls
    path('', home, name="home"),
    url(r'^accounts/', include("accounts.urls")),
    path('post/ajax/applications', db_backup, name="db_backup"),
    path('SuggestApplication/', ajax_load_applications, name='ajax_load_applications'),
    path('SuggestDbInstances/', ajax_load_db_instances, name='ajax_load_db_instances'),
    path('SuggestDatabases/', ajax_load_databases, name='ajax_load_databases'),

    # path('accounts/', include('django.contrib.auth.urls')),
    # path('login/', user_login, name='login'),
    # path('logout/', user_logout, name='logout'),
    # path('change_password/', change_password, name='change_password'),
    path('admin/', admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

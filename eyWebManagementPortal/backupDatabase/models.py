from django.db import models
from django import forms
# Create your models here.


class DbBackupModel(models.Model):
    select_application = models.CharField(max_length=100)
    select_db_instance = models.CharField(max_length=100)
    select_database = models.CharField(max_length=100)
    db_backup_type = models.CharField(max_length=100)

    # def __str__(self):
    #     return self.select_application, self.select_db_instance, self.select_database

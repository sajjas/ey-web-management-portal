from django.contrib import admin
from .models import DbBackupModel

# Register your models here.
admin.site.register(DbBackupModel)
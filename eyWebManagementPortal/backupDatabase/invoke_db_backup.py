# -*- coding: iso-8859-1 -*-
import subprocess, sys, os

def trigger_db_backup(db_instance, backup_db, backup_location, backup_type):
    try:
        backup_powershell_script_path = os.getcwd() + "\\backupDatabase\\powershellUtils\\backup_db.ps1"
        p = subprocess.Popen(["powershell.exe", 
                    backup_powershell_script_path + 
                    ' "' + db_instance + '" ' + 
                    '"' + backup_db + '" ' + 
                    '"' + backup_location + '" ' + 
                    '"' + backup_type + '"'], 
                    stdout=sys.stdout)

        # Print Output of Powershell Command Execution
        output = p.communicate()
    except Exception as e:
        output = e
    
    return output

from django.shortcuts import render, HttpResponse, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.cache import cache_control
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.core import serializers
from .forms import DbBackupForm
from .models import DbBackupModel
import pyodbc, json
from backupDatabase import invoke_db_backup


# def user_login(request):
#     # context = RequestContext(request)
#     _message = ''
#     if request.method == 'POST':
#         username = request.POST['username']
#         password = request.POST['password']
#         user = authenticate(username=username, password=password)
#         if user is not None:
#             if user.is_active:
#                 login(request, user)
#                 # Redirect to home page.
#                 return HttpResponseRedirect(reverse('home'))
#             else:
#                 # Return a 'disabled account' error message
#                 _message = "You're account is disabled."
#         else:
#             # Return an 'invalid login' error message.
#             _message = "Invalid username or password."
#             # return render('login.html', {}, context)
#     # else:
#         # the login is a  GET request, so just show the user the login form.
#     context = {'message': _message}
#     return render(request, 'login.html', context)


# @cache_control(no_cache=True, must_revalidate=True, no_store=True)
# @login_required(login_url='login') #if not logged in redirect to /login
# def user_logout(request):
#     logout(request)
#     # Redirect back to index page.
#     return HttpResponseRedirect(reverse('login'))


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='login') #if not logged in redirect to /login
def home(request):
    form = DbBackupForm()
    db_backup_data = DbBackupModel.objects.all()
    return render(request, "index.html", {"form": form, "db_backup_data": db_backup_data})

def ajax_load_applications(request):
    if request.is_ajax():
        q = request.GET.get('term', '')
        cn = pyodbc.connect('DRIVER={SQL Server};SERVER=LAPTOP-CIIFI19G;UID=sa;PWD=temp')
        cursor = cn.cursor()
        cmd = "select distinct(ModelRegion) from AdventureWorksDW2017.dbo.vTimeSeries where ModelRegion like '%" + q + "%'"
        
        # cn = pyodbc.connect('DRIVER={SQL Server};SERVER=USSECVMPDBTSQ01.ey.net;Trusted_Connection=yes;')
        # cursor = cn.cursor()
        # cmd = "select distinct(AppName) from DBA_CentralDB.dbo.vInstancesList where AppName like '%" + q + "%'"
        
        cursor.execute(cmd)
        projects = cursor.fetchall()
        results = []
        for project in projects:
            project_json = {}
            project_json = project[0]
            results.append(project_json)
        data = json.dumps(results)
    else:
        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)

def ajax_load_db_instances(request):
    if request.is_ajax():
        app_name= request.GET['appName']
        cn = pyodbc.connect('DRIVER={SQL Server};SERVER=LAPTOP-CIIFI19G;UID=sa;PWD=temp')
        cursor = cn.cursor()
        cmd = """select TimeIndex from [AdventureWorksDW2017].[dbo].[vTimeSeries] where ModelRegion ='""" + app_name + """'"""
        
        # cn = pyodbc.connect('DRIVER={SQL Server};SERVER=USSECVMPDBTSQ01.ey.net;Trusted_Connection=yes;')
        # cursor = cn.cursor()
        # cmd = """select distinct(InstanceName) from DBA_CentralDB.dbo.vInstancesList where AppName ='""" + app_name + """'"""
        
        cursor.execute(cmd)
        projects = cursor.fetchall()
        results = []
        for project in projects:
            project_json = {}
            project_json = project[0]
            results.append(project_json)
        data = json.dumps(results)
    else:
        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)

def ajax_load_databases(request):
    if request.is_ajax():
        q = request.GET['db_instance_name']
        cn = pyodbc.connect('DRIVER={SQL Server};SERVER=LAPTOP-CIIFI19G;UID=sa;PWD=temp')
        cursor = cn.cursor()
        cmd = """select distinct(Month) from [AdventureWorksDW2017].[dbo].[vTimeSeries] where TimeIndex ='""" + q + """'"""
        
        # cn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER=USSECVMPMGMSQ03\\INST1;Trusted_Connection=yes;')
        # cursor = cn.cursor()
        # cmd = """select name from master.dbo.sysdatabases where name not in ('master', 'tempdb', 'model', 'msdb')"""
        
        cursor.execute(cmd)
        projects = cursor.fetchall()
        results = []
        for project in projects:
            project_json = {}
            project_json = project[0]
            results.append(project_json)
        data = json.dumps(results)
    else:
        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='login') #if not logged in redirect to /login
def db_backup(request):
    # request should be ajax and method should be POST.
    if request.is_ajax and request.method == "POST":
        # get the form data
        form = DbBackupForm(request.POST)
       
        select_db_instance = request.POST.get('select_db_instance')
        form.fields['select_db_instance'].choices = [(select_db_instance, select_db_instance)]

        select_database = request.POST.get('select_database')
        form.fields['select_database'].choices = [(select_database, select_database)]
        # save the data and after fetch the object in instance
        if form.is_valid():
            instance = form.save()
            # serialize in new friend object in json
            ser_instance = serializers.serialize('json', [instance, ])
            # send to client side.
            
            # perform_db_backup(request.POST.get('select_db_instance'), request.POST.get('select_database'),
            # 'C:\\Windows\\System32\\WindowsPowerShell\\v1.0', 'FULL')

            perform_db_backup('USSECVMPMGMSQ03\\INST1', str(request.POST.get('select_database')), 
            'C:\\Users\\A1263268-3\\Downloads', 'FULL')

            return JsonResponse({"instance": ser_instance}, status=200)
        else:
            # some form errors occured.
            return JsonResponse({"error": form.errors}, status=400)

    # some error occured
    return JsonResponse({"error": ""}, status=400)


def perform_db_backup(db_instance, db_name, db_backup_path, db_backup_type):
    backup_output = invoke_db_backup.trigger_db_backup(db_instance, db_name, db_backup_path, db_backup_type)

    print(backup_output)
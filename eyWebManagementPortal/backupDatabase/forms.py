from .models import DbBackupModel
from django import forms
import json


class DbBackupForm(forms.ModelForm):
    default_choice = [('', '----------')]
    db_backup_type_choices = [('', '----------'), 
                                ('Full', 'Full'), 
                                ('Inc', 'Diff'), 
                                ('T-Log', 'T-Log')]
    select_db_instance = forms.ChoiceField(choices=default_choice)
    select_database = forms.ChoiceField(choices=default_choice)
    db_backup_type = forms.ChoiceField(choices=db_backup_type_choices)

    def __init__(self, *args, **kwargs):
        super(DbBackupForm, self).__init__(*args, **kwargs)
        ## add a "form-control" class to each form input
        ## for enabling bootstrap

        for name in self.fields.keys():
            self.fields[name].widget.attrs.update({
                'class': 'form-control',
            })

    class Meta:
        model = DbBackupModel
        fields = ("__all__")

from django.apps import AppConfig


class BackupdatabaseConfig(AppConfig):
    name = 'backupDatabase'

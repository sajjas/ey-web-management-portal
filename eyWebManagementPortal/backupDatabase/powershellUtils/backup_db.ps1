param (
	$SQLInstance,
	$DBName,
	$SharedFolder,
	$BackupType
)


$Date = Get-Date -format yyyy-MM-dd

If ($BackupType  -eq 'FULL')  {

	# Backup a complete database

	Backup-SqlDatabase -ServerInstance $SQLInstance `
						-Database $DBName `
						-CopyOnly `
						-BackupFile "$($SharedFolder)\$DBName-$date.bak" `
						-CompressionOption On `
						-BackupAction Database `
						-checksum -verbose

  }  ElseIf ($BackupType  -eq 'T-LOG')  {

	# Backup the transaction log

	Backup-SqlDatabase -ServerInstance $SQLInstance `
						-Database $DBName `
						-BackupFile "$($SharedFolder)\$DBName-$date.bak" `
						-CompressionOption On `
						-BackupAction Log `
						-checksum -verbose

  }  ElseIf ($BackupType  -eq 'DIFF')  {

	# Create a differential backup

	Backup-SqlDatabase -ServerInstance $SQLInstance `
						-Database $DBName `
						-BackupFile "$($SharedFolder)\$DBName-$date.bak" `
						-CompressionOption On `
						-BackupAction Database `
						-Incremental `
						-checksum -verbose

  }  Else {

	'Cannot  determine what this is'

} 
